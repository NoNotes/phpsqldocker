FROM php:7.3.6-fpm

# Update and Install Dependencies
RUN apt-get update -y \
	&& apt-get install --no-install-recommends -y \
	git \
	zip \
	unzip \
	nginx \
	pkg-config \
	libssl-dev \
	apt-transport-https \
	locales \
	libc6 \
	unixodbc-dev \
	wget \
	gnupg \
	&& curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - \
	&& curl https://packages.microsoft.com/config/debian/9/prod.list > /etc/apt/sources.list.d/mssql-release.list \
	&& apt-get update -y \
	&& ACCEPT_EULA=Y apt-get install -y \ 
	msodbcsql17 \
	mssql-tools \
	&& apt-get clean

# Fix the php-docker-but-no-php.ini-issue... 
RUN tar x --file=/usr/src/php.tar.xz --directory=/usr/src/  php-7.3.6/php.ini-production php-7.3.6/php.ini-development \
    && cp /usr/src/php-7.3.6/php.ini-production /usr/local/etc/php/php.ini

# Enable bcmath for RabbitMQ
RUN docker-php-ext-install bcmath

# Install MSSQL Driver
ENV PATH "$PATH:/opt/mssql-tools/bin"
RUN echo 'en_US.UTF-8 UTF-8' > /etc/locale.gen \
	&& locale-gen \
	&& pecl install sqlsrv-5.6.1 \
	&& pecl install pdo_sqlsrv-5.6.1 \
	&& echo "extension=sqlsrv.so" >> /usr/local/etc/php/php.ini \
	&& echo "extension=pdo_sqlsrv.so" >> /usr/local/etc/php/php.ini

# Allow Composer to be run as root
ENV COMPOSER_ALLOW_SUPERUSER 1

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer